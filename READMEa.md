travail d'apres tuto js apres plusieurs essai avec le langage java. J'ai choisie par manque de temps de me concentrer sur la faille de sécurité xss seulement.
Pour corriger la faille grace a mes autres essai j'ai pu decouvrir différentes méthodes comme ;
 

            __XSS__
__LANGAGE js_____________________________________________________________________________________________________________
Filtre magic_quotes_gpc

Lorsque la fonction magic_quotes est activée, tous les caractères ' (apostrophes), " (guillemets), (antislash) et NULL sont  remplacés par un antislash. La fonction "magic_quotes_gpc" permet de protéger les données envoyées par les méthodes "GET", "POST.
Par exemple si je j'entre le code suivant dans la barre de recherche et je clique sur envoyer:

<script> alert("Hack")</script>

Il devient:

<script>alert(\"hack\")</script>

Plusieurs techniques permettent se se protéger de la faille XSS :

            -htmlspecialchars() 
    convertit les caractères spéciaux en entités HTML.
    
           -htmlentities() 
    qui est identique à htmlspecialchars() sauf qu'elle filtre tout les caractères équivalents au codage html ou javascript.

           -strip_tags() 
    cette fonction supprime toutes les balises.

Mettre des condition de taille et de caractères afin de validé la saisie
Mettre aussi des conditions de rappel et forme (pour les email ------@---)

___________________________________________________________________________________________________________________________
__LANGAGE JAVA_____________________________________________________________________________________________________________
 * Valide l'adresse mail saisie.
 */
private void validationEmail( String email ) throws Exception {
    if ( email != null && email.trim().length() != 0 ) {
        if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
            throw new Exception( "Merci de saisir une adresse mail valide." );
        }
    } else {
        throw new Exception( "Merci de saisir une adresse mail." );
    }
}

/**
 * Valide les mots de passe saisis.
 */
private void validationMotsDePasse( String motDePasse, String confirmation ) throws Exception{
    if (motDePasse != null && motDePasse.trim().length() != 0 && confirmation != null && confirmation.trim().length() != 0) {
        if (!motDePasse.equals(confirmation)) {
            throw new Exception("Les mots de passe entrés sont différents, merci de les saisir à nouveau.");
        } else if (motDePasse.trim().length() < 3) {
            throw new Exception("Les mots de passe doivent contenir au moins 3 caractères.");
        }
    } else {
        throw new Exception("Merci de saisir et confirmer votre mot de passe.");
    }
}
___________________________________________________________________________________________________________________________


            __SQLi__
Pour évité les SQLi il faut utilisé des requetes préparé qui évite les injection de code
_____Exemple ______________________________________________________________________________________________________________
var sql = "SELECT * FROM table WHERE userid = ?";
var inserts = [message.author.id];
sql = mysql.format(sql, inserts);
___________________________________________________________________________________________________________________________
